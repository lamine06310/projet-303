# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "assembly_control_ros: 14 messages, 0 services")

set(MSG_I_FLAGS "-Iassembly_control_ros:/home/robuser/projet_303/wrks/src/assembly_control_ros/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(assembly_control_ros_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg" ""
)

get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg" NAME_WE)
add_custom_target(_assembly_control_ros_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "assembly_control_ros" "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_cpp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
)

### Generating Services

### Generating Module File
_generate_module_cpp(assembly_control_ros
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(assembly_control_ros_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(assembly_control_ros_generate_messages assembly_control_ros_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_cpp _assembly_control_ros_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(assembly_control_ros_gencpp)
add_dependencies(assembly_control_ros_gencpp assembly_control_ros_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS assembly_control_ros_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_eus(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
)

### Generating Services

### Generating Module File
_generate_module_eus(assembly_control_ros
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(assembly_control_ros_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(assembly_control_ros_generate_messages assembly_control_ros_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_eus _assembly_control_ros_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(assembly_control_ros_geneus)
add_dependencies(assembly_control_ros_geneus assembly_control_ros_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS assembly_control_ros_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_lisp(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
)

### Generating Services

### Generating Module File
_generate_module_lisp(assembly_control_ros
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(assembly_control_ros_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(assembly_control_ros_generate_messages assembly_control_ros_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_lisp _assembly_control_ros_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(assembly_control_ros_genlisp)
add_dependencies(assembly_control_ros_genlisp assembly_control_ros_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS assembly_control_ros_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_nodejs(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
)

### Generating Services

### Generating Module File
_generate_module_nodejs(assembly_control_ros
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(assembly_control_ros_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(assembly_control_ros_generate_messages assembly_control_ros_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_nodejs _assembly_control_ros_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(assembly_control_ros_gennodejs)
add_dependencies(assembly_control_ros_gennodejs assembly_control_ros_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS assembly_control_ros_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)
_generate_msg_py(assembly_control_ros
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
)

### Generating Services

### Generating Module File
_generate_module_py(assembly_control_ros
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(assembly_control_ros_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(assembly_control_ros_generate_messages assembly_control_ros_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/assembly_station_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/evacuation_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_command.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/camera_output.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/robot_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_state.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/robuser/projet_303/wrks/src/assembly_control_ros/msg/supply_conveyor_input.msg" NAME_WE)
add_dependencies(assembly_control_ros_generate_messages_py _assembly_control_ros_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(assembly_control_ros_genpy)
add_dependencies(assembly_control_ros_genpy assembly_control_ros_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS assembly_control_ros_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/assembly_control_ros
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(assembly_control_ros_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/assembly_control_ros
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(assembly_control_ros_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/assembly_control_ros
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(assembly_control_ros_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/assembly_control_ros
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(assembly_control_ros_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/assembly_control_ros
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(assembly_control_ros_generate_messages_py std_msgs_generate_messages_py)
endif()
