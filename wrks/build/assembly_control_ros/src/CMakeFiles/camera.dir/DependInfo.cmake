# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/src/camera/main.cpp" "/home/robuser/projet_303/wrks/build/assembly_control_ros/src/CMakeFiles/camera.dir/camera/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"assembly_control_ros\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/robuser/projet_303/wrks/devel/include"
  "/home/robuser/projet_303/wrks/src/assembly_control_ros/src/../include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
