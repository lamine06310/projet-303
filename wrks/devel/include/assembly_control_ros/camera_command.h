// Generated by gencpp from file assembly_control_ros/camera_command.msg
// DO NOT EDIT!


#ifndef ASSEMBLY_CONTROL_ROS_MESSAGE_CAMERA_COMMAND_H
#define ASSEMBLY_CONTROL_ROS_MESSAGE_CAMERA_COMMAND_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace assembly_control_ros
{
template <class ContainerAllocator>
struct camera_command_
{
  typedef camera_command_<ContainerAllocator> Type;

  camera_command_()
    : process(false)  {
    }
  camera_command_(const ContainerAllocator& _alloc)
    : process(false)  {
  (void)_alloc;
    }



   typedef uint8_t _process_type;
  _process_type process;





  typedef boost::shared_ptr< ::assembly_control_ros::camera_command_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::assembly_control_ros::camera_command_<ContainerAllocator> const> ConstPtr;

}; // struct camera_command_

typedef ::assembly_control_ros::camera_command_<std::allocator<void> > camera_command;

typedef boost::shared_ptr< ::assembly_control_ros::camera_command > camera_commandPtr;
typedef boost::shared_ptr< ::assembly_control_ros::camera_command const> camera_commandConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::assembly_control_ros::camera_command_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::assembly_control_ros::camera_command_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace assembly_control_ros

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/melodic/share/std_msgs/cmake/../msg'], 'assembly_control_ros': ['/home/robuser/projet_303/wrks/src/assembly_control_ros/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::assembly_control_ros::camera_command_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::assembly_control_ros::camera_command_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::assembly_control_ros::camera_command_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::assembly_control_ros::camera_command_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::assembly_control_ros::camera_command_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::assembly_control_ros::camera_command_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::assembly_control_ros::camera_command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "1c650d1d3c30589cc2658a353420cbf2";
  }

  static const char* value(const ::assembly_control_ros::camera_command_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x1c650d1d3c30589cULL;
  static const uint64_t static_value2 = 0xc2658a353420cbf2ULL;
};

template<class ContainerAllocator>
struct DataType< ::assembly_control_ros::camera_command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "assembly_control_ros/camera_command";
  }

  static const char* value(const ::assembly_control_ros::camera_command_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::assembly_control_ros::camera_command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool process\n"
;
  }

  static const char* value(const ::assembly_control_ros::camera_command_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::assembly_control_ros::camera_command_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.process);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct camera_command_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::assembly_control_ros::camera_command_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::assembly_control_ros::camera_command_<ContainerAllocator>& v)
  {
    s << indent << "process: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.process);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ASSEMBLY_CONTROL_ROS_MESSAGE_CAMERA_COMMAND_H
