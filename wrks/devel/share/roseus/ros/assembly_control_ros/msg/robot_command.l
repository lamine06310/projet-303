;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::robot_command)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'robot_command (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ROBOT_COMMAND")
  (make-package "ASSEMBLY_CONTROL_ROS::ROBOT_COMMAND"))

(in-package "ROS")
;;//! \htmlinclude robot_command.msg.html


(defclass assembly_control_ros::robot_command
  :super ros::object
  :slots (_move_right _move_left _grasp _release _assemble_part1 _assemble_part2 _assemble_part3 ))

(defmethod assembly_control_ros::robot_command
  (:init
   (&key
    ((:move_right __move_right) nil)
    ((:move_left __move_left) nil)
    ((:grasp __grasp) nil)
    ((:release __release) nil)
    ((:assemble_part1 __assemble_part1) nil)
    ((:assemble_part2 __assemble_part2) nil)
    ((:assemble_part3 __assemble_part3) nil)
    )
   (send-super :init)
   (setq _move_right __move_right)
   (setq _move_left __move_left)
   (setq _grasp __grasp)
   (setq _release __release)
   (setq _assemble_part1 __assemble_part1)
   (setq _assemble_part2 __assemble_part2)
   (setq _assemble_part3 __assemble_part3)
   self)
  (:move_right
   (&optional __move_right)
   (if __move_right (setq _move_right __move_right)) _move_right)
  (:move_left
   (&optional __move_left)
   (if __move_left (setq _move_left __move_left)) _move_left)
  (:grasp
   (&optional __grasp)
   (if __grasp (setq _grasp __grasp)) _grasp)
  (:release
   (&optional __release)
   (if __release (setq _release __release)) _release)
  (:assemble_part1
   (&optional __assemble_part1)
   (if __assemble_part1 (setq _assemble_part1 __assemble_part1)) _assemble_part1)
  (:assemble_part2
   (&optional __assemble_part2)
   (if __assemble_part2 (setq _assemble_part2 __assemble_part2)) _assemble_part2)
  (:assemble_part3
   (&optional __assemble_part3)
   (if __assemble_part3 (setq _assemble_part3 __assemble_part3)) _assemble_part3)
  (:serialization-length
   ()
   (+
    ;; bool _move_right
    1
    ;; bool _move_left
    1
    ;; bool _grasp
    1
    ;; bool _release
    1
    ;; bool _assemble_part1
    1
    ;; bool _assemble_part2
    1
    ;; bool _assemble_part3
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _move_right
       (if _move_right (write-byte -1 s) (write-byte 0 s))
     ;; bool _move_left
       (if _move_left (write-byte -1 s) (write-byte 0 s))
     ;; bool _grasp
       (if _grasp (write-byte -1 s) (write-byte 0 s))
     ;; bool _release
       (if _release (write-byte -1 s) (write-byte 0 s))
     ;; bool _assemble_part1
       (if _assemble_part1 (write-byte -1 s) (write-byte 0 s))
     ;; bool _assemble_part2
       (if _assemble_part2 (write-byte -1 s) (write-byte 0 s))
     ;; bool _assemble_part3
       (if _assemble_part3 (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _move_right
     (setq _move_right (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _move_left
     (setq _move_left (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _grasp
     (setq _grasp (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _release
     (setq _release (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _assemble_part1
     (setq _assemble_part1 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _assemble_part2
     (setq _assemble_part2 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _assemble_part3
     (setq _assemble_part3 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::robot_command :md5sum-) "90dd75667caf471745cdc1c8deb81f71")
(setf (get assembly_control_ros::robot_command :datatype-) "assembly_control_ros/robot_command")
(setf (get assembly_control_ros::robot_command :definition-)
      "bool move_right
bool move_left
bool grasp
bool release
bool assemble_part1
bool assemble_part2
bool assemble_part3
")



(provide :assembly_control_ros/robot_command "90dd75667caf471745cdc1c8deb81f71")


