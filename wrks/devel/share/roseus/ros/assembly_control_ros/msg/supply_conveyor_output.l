;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::supply_conveyor_output)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'supply_conveyor_output (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_OUTPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_OUTPUT"))

(in-package "ROS")
;;//! \htmlinclude supply_conveyor_output.msg.html


(defclass assembly_control_ros::supply_conveyor_output
  :super ros::object
  :slots (_part_available ))

(defmethod assembly_control_ros::supply_conveyor_output
  (:init
   (&key
    ((:part_available __part_available) nil)
    )
   (send-super :init)
   (setq _part_available __part_available)
   self)
  (:part_available
   (&optional __part_available)
   (if __part_available (setq _part_available __part_available)) _part_available)
  (:serialization-length
   ()
   (+
    ;; bool _part_available
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _part_available
       (if _part_available (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _part_available
     (setq _part_available (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::supply_conveyor_output :md5sum-) "380de1277a09f4e060dd938224d3c515")
(setf (get assembly_control_ros::supply_conveyor_output :datatype-) "assembly_control_ros/supply_conveyor_output")
(setf (get assembly_control_ros::supply_conveyor_output :definition-)
      "bool part_available
")



(provide :assembly_control_ros/supply_conveyor_output "380de1277a09f4e060dd938224d3c515")


