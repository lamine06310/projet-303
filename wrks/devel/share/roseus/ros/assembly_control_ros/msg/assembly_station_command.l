;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::assembly_station_command)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'assembly_station_command (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_COMMAND")
  (make-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_COMMAND"))

(in-package "ROS")
;;//! \htmlinclude assembly_station_command.msg.html


(defclass assembly_control_ros::assembly_station_command
  :super ros::object
  :slots (_check ))

(defmethod assembly_control_ros::assembly_station_command
  (:init
   (&key
    ((:check __check) nil)
    )
   (send-super :init)
   (setq _check __check)
   self)
  (:check
   (&optional __check)
   (if __check (setq _check __check)) _check)
  (:serialization-length
   ()
   (+
    ;; bool _check
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _check
       (if _check (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _check
     (setq _check (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::assembly_station_command :md5sum-) "c5df00fea9d1f39520fa0345cbde1b26")
(setf (get assembly_control_ros::assembly_station_command :datatype-) "assembly_control_ros/assembly_station_command")
(setf (get assembly_control_ros::assembly_station_command :definition-)
      "bool check
")



(provide :assembly_control_ros/assembly_station_command "c5df00fea9d1f39520fa0345cbde1b26")


