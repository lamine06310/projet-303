;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::camera_input)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'camera_input (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::CAMERA_INPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::CAMERA_INPUT"))

(in-package "ROS")
;;//! \htmlinclude camera_input.msg.html


(defclass assembly_control_ros::camera_input
  :super ros::object
  :slots (_start_recognition ))

(defmethod assembly_control_ros::camera_input
  (:init
   (&key
    ((:start_recognition __start_recognition) nil)
    )
   (send-super :init)
   (setq _start_recognition __start_recognition)
   self)
  (:start_recognition
   (&optional __start_recognition)
   (if __start_recognition (setq _start_recognition __start_recognition)) _start_recognition)
  (:serialization-length
   ()
   (+
    ;; bool _start_recognition
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _start_recognition
       (if _start_recognition (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _start_recognition
     (setq _start_recognition (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::camera_input :md5sum-) "ce459b5e113c1fb16ff60052a520a482")
(setf (get assembly_control_ros::camera_input :datatype-) "assembly_control_ros/camera_input")
(setf (get assembly_control_ros::camera_input :definition-)
      "bool start_recognition
")



(provide :assembly_control_ros/camera_input "ce459b5e113c1fb16ff60052a520a482")


