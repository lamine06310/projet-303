;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::assembly_station_state)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'assembly_station_state (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_STATE")
  (make-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_STATE"))

(in-package "ROS")
;;//! \htmlinclude assembly_station_state.msg.html


(defclass assembly_control_ros::assembly_station_state
  :super ros::object
  :slots (_valid _evacuated ))

(defmethod assembly_control_ros::assembly_station_state
  (:init
   (&key
    ((:valid __valid) nil)
    ((:evacuated __evacuated) nil)
    )
   (send-super :init)
   (setq _valid __valid)
   (setq _evacuated __evacuated)
   self)
  (:valid
   (&optional __valid)
   (if __valid (setq _valid __valid)) _valid)
  (:evacuated
   (&optional __evacuated)
   (if __evacuated (setq _evacuated __evacuated)) _evacuated)
  (:serialization-length
   ()
   (+
    ;; bool _valid
    1
    ;; bool _evacuated
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _valid
       (if _valid (write-byte -1 s) (write-byte 0 s))
     ;; bool _evacuated
       (if _evacuated (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _valid
     (setq _valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _evacuated
     (setq _evacuated (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::assembly_station_state :md5sum-) "5a5113dbe54fee79575bff785c5ca0bc")
(setf (get assembly_control_ros::assembly_station_state :datatype-) "assembly_control_ros/assembly_station_state")
(setf (get assembly_control_ros::assembly_station_state :definition-)
      "bool valid
bool evacuated
")



(provide :assembly_control_ros/assembly_station_state "5a5113dbe54fee79575bff785c5ca0bc")


