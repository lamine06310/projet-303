;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::camera_command)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'camera_command (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::CAMERA_COMMAND")
  (make-package "ASSEMBLY_CONTROL_ROS::CAMERA_COMMAND"))

(in-package "ROS")
;;//! \htmlinclude camera_command.msg.html


(defclass assembly_control_ros::camera_command
  :super ros::object
  :slots (_process ))

(defmethod assembly_control_ros::camera_command
  (:init
   (&key
    ((:process __process) nil)
    )
   (send-super :init)
   (setq _process __process)
   self)
  (:process
   (&optional __process)
   (if __process (setq _process __process)) _process)
  (:serialization-length
   ()
   (+
    ;; bool _process
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _process
       (if _process (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _process
     (setq _process (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::camera_command :md5sum-) "1c650d1d3c30589cc2658a353420cbf2")
(setf (get assembly_control_ros::camera_command :datatype-) "assembly_control_ros/camera_command")
(setf (get assembly_control_ros::camera_command :definition-)
      "bool process
")



(provide :assembly_control_ros/camera_command "1c650d1d3c30589cc2658a353420cbf2")


