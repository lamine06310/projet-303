;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::supply_conveyor_command)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'supply_conveyor_command (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_COMMAND")
  (make-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_COMMAND"))

(in-package "ROS")
;;//! \htmlinclude supply_conveyor_command.msg.html


(defclass assembly_control_ros::supply_conveyor_command
  :super ros::object
  :slots (_on ))

(defmethod assembly_control_ros::supply_conveyor_command
  (:init
   (&key
    ((:on __on) nil)
    )
   (send-super :init)
   (setq _on __on)
   self)
  (:on
   (&optional __on)
   (if __on (setq _on __on)) _on)
  (:serialization-length
   ()
   (+
    ;; bool _on
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _on
       (if _on (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _on
     (setq _on (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::supply_conveyor_command :md5sum-) "74983d2ffe4877de8ae30b7a94625c41")
(setf (get assembly_control_ros::supply_conveyor_command :datatype-) "assembly_control_ros/supply_conveyor_command")
(setf (get assembly_control_ros::supply_conveyor_command :definition-)
      "bool on
")



(provide :assembly_control_ros/supply_conveyor_command "74983d2ffe4877de8ae30b7a94625c41")


