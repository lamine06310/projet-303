// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class evacuation_conveyor_state {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.stopped = null;
    }
    else {
      if (initObj.hasOwnProperty('stopped')) {
        this.stopped = initObj.stopped
      }
      else {
        this.stopped = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type evacuation_conveyor_state
    // Serialize message field [stopped]
    bufferOffset = _serializer.bool(obj.stopped, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type evacuation_conveyor_state
    let len;
    let data = new evacuation_conveyor_state(null);
    // Deserialize message field [stopped]
    data.stopped = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/evacuation_conveyor_state';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'caab21a24341159709db962b47b01ad1';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool stopped
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new evacuation_conveyor_state(null);
    if (msg.stopped !== undefined) {
      resolved.stopped = msg.stopped;
    }
    else {
      resolved.stopped = false
    }

    return resolved;
    }
};

module.exports = evacuation_conveyor_state;
