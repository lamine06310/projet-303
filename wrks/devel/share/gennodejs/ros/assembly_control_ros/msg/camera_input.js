// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class camera_input {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.start_recognition = null;
    }
    else {
      if (initObj.hasOwnProperty('start_recognition')) {
        this.start_recognition = initObj.start_recognition
      }
      else {
        this.start_recognition = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type camera_input
    // Serialize message field [start_recognition]
    bufferOffset = _serializer.bool(obj.start_recognition, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type camera_input
    let len;
    let data = new camera_input(null);
    // Deserialize message field [start_recognition]
    data.start_recognition = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/camera_input';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ce459b5e113c1fb16ff60052a520a482';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool start_recognition
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new camera_input(null);
    if (msg.start_recognition !== undefined) {
      resolved.start_recognition = msg.start_recognition;
    }
    else {
      resolved.start_recognition = false
    }

    return resolved;
    }
};

module.exports = camera_input;
