// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class supply_conveyor_input {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.restart = null;
    }
    else {
      if (initObj.hasOwnProperty('restart')) {
        this.restart = initObj.restart
      }
      else {
        this.restart = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type supply_conveyor_input
    // Serialize message field [restart]
    bufferOffset = _serializer.bool(obj.restart, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type supply_conveyor_input
    let len;
    let data = new supply_conveyor_input(null);
    // Deserialize message field [restart]
    data.restart = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/supply_conveyor_input';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b8cfd5cfa335ea9b5b24ce2c119bfcd6';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool restart
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new supply_conveyor_input(null);
    if (msg.restart !== undefined) {
      resolved.restart = msg.restart;
    }
    else {
      resolved.restart = false
    }

    return resolved;
    }
};

module.exports = supply_conveyor_input;
