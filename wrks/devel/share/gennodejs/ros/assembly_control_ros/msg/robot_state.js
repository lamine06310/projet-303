// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class robot_state {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.part_grasped = null;
      this.part_released = null;
      this.at_supply_conveyor = null;
      this.at_evacuation_conveyor = null;
      this.at_assembly_station = null;
      this.part1_assembled = null;
      this.part2_assembled = null;
      this.part3_assembled = null;
    }
    else {
      if (initObj.hasOwnProperty('part_grasped')) {
        this.part_grasped = initObj.part_grasped
      }
      else {
        this.part_grasped = false;
      }
      if (initObj.hasOwnProperty('part_released')) {
        this.part_released = initObj.part_released
      }
      else {
        this.part_released = false;
      }
      if (initObj.hasOwnProperty('at_supply_conveyor')) {
        this.at_supply_conveyor = initObj.at_supply_conveyor
      }
      else {
        this.at_supply_conveyor = false;
      }
      if (initObj.hasOwnProperty('at_evacuation_conveyor')) {
        this.at_evacuation_conveyor = initObj.at_evacuation_conveyor
      }
      else {
        this.at_evacuation_conveyor = false;
      }
      if (initObj.hasOwnProperty('at_assembly_station')) {
        this.at_assembly_station = initObj.at_assembly_station
      }
      else {
        this.at_assembly_station = false;
      }
      if (initObj.hasOwnProperty('part1_assembled')) {
        this.part1_assembled = initObj.part1_assembled
      }
      else {
        this.part1_assembled = false;
      }
      if (initObj.hasOwnProperty('part2_assembled')) {
        this.part2_assembled = initObj.part2_assembled
      }
      else {
        this.part2_assembled = false;
      }
      if (initObj.hasOwnProperty('part3_assembled')) {
        this.part3_assembled = initObj.part3_assembled
      }
      else {
        this.part3_assembled = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type robot_state
    // Serialize message field [part_grasped]
    bufferOffset = _serializer.bool(obj.part_grasped, buffer, bufferOffset);
    // Serialize message field [part_released]
    bufferOffset = _serializer.bool(obj.part_released, buffer, bufferOffset);
    // Serialize message field [at_supply_conveyor]
    bufferOffset = _serializer.bool(obj.at_supply_conveyor, buffer, bufferOffset);
    // Serialize message field [at_evacuation_conveyor]
    bufferOffset = _serializer.bool(obj.at_evacuation_conveyor, buffer, bufferOffset);
    // Serialize message field [at_assembly_station]
    bufferOffset = _serializer.bool(obj.at_assembly_station, buffer, bufferOffset);
    // Serialize message field [part1_assembled]
    bufferOffset = _serializer.bool(obj.part1_assembled, buffer, bufferOffset);
    // Serialize message field [part2_assembled]
    bufferOffset = _serializer.bool(obj.part2_assembled, buffer, bufferOffset);
    // Serialize message field [part3_assembled]
    bufferOffset = _serializer.bool(obj.part3_assembled, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type robot_state
    let len;
    let data = new robot_state(null);
    // Deserialize message field [part_grasped]
    data.part_grasped = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [part_released]
    data.part_released = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [at_supply_conveyor]
    data.at_supply_conveyor = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [at_evacuation_conveyor]
    data.at_evacuation_conveyor = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [at_assembly_station]
    data.at_assembly_station = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [part1_assembled]
    data.part1_assembled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [part2_assembled]
    data.part2_assembled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [part3_assembled]
    data.part3_assembled = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 8;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/robot_state';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'fc0a5945651ca653c6fce56198225a07';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool part_grasped
    bool part_released
    bool at_supply_conveyor
    bool at_evacuation_conveyor
    bool at_assembly_station
    bool part1_assembled
    bool part2_assembled
    bool part3_assembled
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new robot_state(null);
    if (msg.part_grasped !== undefined) {
      resolved.part_grasped = msg.part_grasped;
    }
    else {
      resolved.part_grasped = false
    }

    if (msg.part_released !== undefined) {
      resolved.part_released = msg.part_released;
    }
    else {
      resolved.part_released = false
    }

    if (msg.at_supply_conveyor !== undefined) {
      resolved.at_supply_conveyor = msg.at_supply_conveyor;
    }
    else {
      resolved.at_supply_conveyor = false
    }

    if (msg.at_evacuation_conveyor !== undefined) {
      resolved.at_evacuation_conveyor = msg.at_evacuation_conveyor;
    }
    else {
      resolved.at_evacuation_conveyor = false
    }

    if (msg.at_assembly_station !== undefined) {
      resolved.at_assembly_station = msg.at_assembly_station;
    }
    else {
      resolved.at_assembly_station = false
    }

    if (msg.part1_assembled !== undefined) {
      resolved.part1_assembled = msg.part1_assembled;
    }
    else {
      resolved.part1_assembled = false
    }

    if (msg.part2_assembled !== undefined) {
      resolved.part2_assembled = msg.part2_assembled;
    }
    else {
      resolved.part2_assembled = false
    }

    if (msg.part3_assembled !== undefined) {
      resolved.part3_assembled = msg.part3_assembled;
    }
    else {
      resolved.part3_assembled = false
    }

    return resolved;
    }
};

module.exports = robot_state;
