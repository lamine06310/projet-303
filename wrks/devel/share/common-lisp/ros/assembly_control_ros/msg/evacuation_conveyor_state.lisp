; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude evacuation_conveyor_state.msg.html

(cl:defclass <evacuation_conveyor_state> (roslisp-msg-protocol:ros-message)
  ((stopped
    :reader stopped
    :initarg :stopped
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass evacuation_conveyor_state (<evacuation_conveyor_state>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <evacuation_conveyor_state>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'evacuation_conveyor_state)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<evacuation_conveyor_state> is deprecated: use assembly_control_ros-msg:evacuation_conveyor_state instead.")))

(cl:ensure-generic-function 'stopped-val :lambda-list '(m))
(cl:defmethod stopped-val ((m <evacuation_conveyor_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:stopped-val is deprecated.  Use assembly_control_ros-msg:stopped instead.")
  (stopped m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <evacuation_conveyor_state>) ostream)
  "Serializes a message object of type '<evacuation_conveyor_state>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stopped) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <evacuation_conveyor_state>) istream)
  "Deserializes a message object of type '<evacuation_conveyor_state>"
    (cl:setf (cl:slot-value msg 'stopped) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<evacuation_conveyor_state>)))
  "Returns string type for a message object of type '<evacuation_conveyor_state>"
  "assembly_control_ros/evacuation_conveyor_state")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'evacuation_conveyor_state)))
  "Returns string type for a message object of type 'evacuation_conveyor_state"
  "assembly_control_ros/evacuation_conveyor_state")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<evacuation_conveyor_state>)))
  "Returns md5sum for a message object of type '<evacuation_conveyor_state>"
  "caab21a24341159709db962b47b01ad1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'evacuation_conveyor_state)))
  "Returns md5sum for a message object of type 'evacuation_conveyor_state"
  "caab21a24341159709db962b47b01ad1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<evacuation_conveyor_state>)))
  "Returns full string definition for message of type '<evacuation_conveyor_state>"
  (cl:format cl:nil "bool stopped~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'evacuation_conveyor_state)))
  "Returns full string definition for message of type 'evacuation_conveyor_state"
  (cl:format cl:nil "bool stopped~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <evacuation_conveyor_state>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <evacuation_conveyor_state>))
  "Converts a ROS message object to a list"
  (cl:list 'evacuation_conveyor_state
    (cl:cons ':stopped (stopped msg))
))
