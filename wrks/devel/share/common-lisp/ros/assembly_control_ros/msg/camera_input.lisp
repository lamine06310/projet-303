; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude camera_input.msg.html

(cl:defclass <camera_input> (roslisp-msg-protocol:ros-message)
  ((start_recognition
    :reader start_recognition
    :initarg :start_recognition
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass camera_input (<camera_input>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <camera_input>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'camera_input)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<camera_input> is deprecated: use assembly_control_ros-msg:camera_input instead.")))

(cl:ensure-generic-function 'start_recognition-val :lambda-list '(m))
(cl:defmethod start_recognition-val ((m <camera_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:start_recognition-val is deprecated.  Use assembly_control_ros-msg:start_recognition instead.")
  (start_recognition m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <camera_input>) ostream)
  "Serializes a message object of type '<camera_input>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'start_recognition) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <camera_input>) istream)
  "Deserializes a message object of type '<camera_input>"
    (cl:setf (cl:slot-value msg 'start_recognition) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<camera_input>)))
  "Returns string type for a message object of type '<camera_input>"
  "assembly_control_ros/camera_input")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'camera_input)))
  "Returns string type for a message object of type 'camera_input"
  "assembly_control_ros/camera_input")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<camera_input>)))
  "Returns md5sum for a message object of type '<camera_input>"
  "ce459b5e113c1fb16ff60052a520a482")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'camera_input)))
  "Returns md5sum for a message object of type 'camera_input"
  "ce459b5e113c1fb16ff60052a520a482")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<camera_input>)))
  "Returns full string definition for message of type '<camera_input>"
  (cl:format cl:nil "bool start_recognition~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'camera_input)))
  "Returns full string definition for message of type 'camera_input"
  (cl:format cl:nil "bool start_recognition~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <camera_input>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <camera_input>))
  "Converts a ROS message object to a list"
  (cl:list 'camera_input
    (cl:cons ':start_recognition (start_recognition msg))
))
