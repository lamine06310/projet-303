# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from assembly_control_ros/camera_output.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct


class camera_output(genpy.Message):
  _md5sum = "f03ae215b98c387da3ca20f4eef0f6bf"
  _type = "assembly_control_ros/camera_output"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """bool part_analyzed
bool part1
bool part2
bool part3"""
  __slots__ = ['part_analyzed','part1','part2','part3']
  _slot_types = ['bool','bool','bool','bool']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       part_analyzed,part1,part2,part3

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(camera_output, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.part_analyzed is None:
        self.part_analyzed = False
      if self.part1 is None:
        self.part1 = False
      if self.part2 is None:
        self.part2 = False
      if self.part3 is None:
        self.part3 = False
    else:
      self.part_analyzed = False
      self.part1 = False
      self.part2 = False
      self.part3 = False

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_get_struct_4B().pack(_x.part_analyzed, _x.part1, _x.part2, _x.part3))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      end = 0
      _x = self
      start = end
      end += 4
      (_x.part_analyzed, _x.part1, _x.part2, _x.part3,) = _get_struct_4B().unpack(str[start:end])
      self.part_analyzed = bool(self.part_analyzed)
      self.part1 = bool(self.part1)
      self.part2 = bool(self.part2)
      self.part3 = bool(self.part3)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_get_struct_4B().pack(_x.part_analyzed, _x.part1, _x.part2, _x.part3))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      end = 0
      _x = self
      start = end
      end += 4
      (_x.part_analyzed, _x.part1, _x.part2, _x.part3,) = _get_struct_4B().unpack(str[start:end])
      self.part_analyzed = bool(self.part_analyzed)
      self.part1 = bool(self.part1)
      self.part2 = bool(self.part2)
      self.part3 = bool(self.part3)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
def _get_struct_I():
    global _struct_I
    return _struct_I
_struct_4B = None
def _get_struct_4B():
    global _struct_4B
    if _struct_4B is None:
        _struct_4B = struct.Struct("<4B")
    return _struct_4B
